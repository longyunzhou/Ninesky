﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Ninesky.Web
{
    public class ApiClient
    {
        private HttpClient client { get; set; }

        public static string server { get; set; }

        public string jsonWebTokens { get; set; }

        public ApiClient(string serverBase,string jwt)
        {
            server = serverBase;
            jsonWebTokens = jwt;
            client = new HttpClient();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">返回的数据类型</typeparam>
        /// <param name="method">方法</param>
        /// <param name="path">路径</param>
        /// <param name="content">发送的内容</param>
        /// <returns></returns>
        //public async Task<ApiResponseMessage<T>> SendJsonAsync<T>(HttpMethod method, string path, string content = null) where T : class
        //{
        //    var apiResponseMessage = new ApiResponseMessage<T>();
        //    HttpContent httpContent = null;
        //    if (!string.IsNullOrEmpty(content)) httpContent = new StringContent(content);
        //    var stringMessage = await SendAsync<string>(method, server + path, jsonWebTokens, httpContent);
        //    if(!apiResponseMessage.Successed && apiResponseMessage.StatusCode == System.Net.HttpStatusCode.Unauthorized)
        //    {
        //        //未获取Token
        //        if (string.IsNullOrEmpty(jsonWebTokens))
        //        {
        //            var anonymousPesponseMessage = await SendAsync<string>(HttpMethod.Get, server + "User/GetToken");
        //            if (anonymousPesponseMessage.Successed) jsonWebTokens = anonymousPesponseMessage.Content;
        //            stringMessage = await SendAsync<string>(method, server + path, jsonWebTokens, httpContent);
        //        }
        //    }
        //    apiResponseMessage.Jwt = stringMessage.Jwt;
        //    apiResponseMessage.ReasonPhrase = stringMessage.ReasonPhrase;
        //    apiResponseMessage.StatusCode = stringMessage.StatusCode;
        //    apiResponseMessage.Successed = stringMessage.Successed;
        //    if (stringMessage.Successed) apiResponseMessage.Content = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(stringMessage.Content);
        //    return apiResponseMessage;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">返回内容的类型</typeparam>
        /// <param name="method">方式</param>
        /// <param name="url">地址【完整地址,含server和path】</param>
        /// <param name="jwt">jwt字符串</param>
        /// <param name="content">请求内容</param>
        /// <returns></returns>
        //private async Task<ApiResponseMessage<T>> SendAsync<T>(HttpMethod method, string url, string jwt = null, HttpContent content = null) where T : class
        //{
        //    var stringMessage = new ApiResponseMessage<T>();
        //    HttpRequestMessage requestMessage = new HttpRequestMessage(method, url);
        //    requestMessage.Content = content;
        //    if (!string.IsNullOrWhiteSpace(jwt)) requestMessage.Headers.Add("Authorization", "Bearer " + jwt);
        //    HttpResponseMessage message = new HttpResponseMessage();
        //    try
        //    {
        //        message = await client.SendAsync(requestMessage);
        //        Type type = typeof(T);
        //        if (type.Equals(typeof(string))) stringMessage.Content = await message.Content.ReadAsStringAsync() as T;
        //        else if (type.Equals(typeof(Stream))) stringMessage.Content = await message.Content.ReadAsStreamAsync() as T;
        //        else if (type.Equals(typeof(byte[]))) stringMessage.Content = await message.Content.ReadAsByteArrayAsync() as T;
        //        else
        //        {
        //            stringMessage.Successed = false;
        //            stringMessage.ReasonPhrase = "泛型类型错误";
        //            return stringMessage;
        //        }
        //        //
        //        stringMessage.StatusCode = message.StatusCode;
        //        stringMessage.Successed = message.IsSuccessStatusCode;
        //        stringMessage.ReasonPhrase = message.ReasonPhrase;

        //        var authorization = message.Headers.FirstOrDefault(h => h.Key == "Authorization").Value.FirstOrDefault(c => c.StartsWith("Bearer"));
        //        if (string.IsNullOrEmpty(authorization))
        //        {
        //            stringMessage.Jwt = authorization.Substring("Bearer ".Length).Trim();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        stringMessage.Successed = false;
        //        stringMessage.StatusCode = System.Net.HttpStatusCode.InternalServerError;
        //        stringMessage.ReasonPhrase = ex.Message;
        //    }
        //    return stringMessage;
        //}
    }
}
