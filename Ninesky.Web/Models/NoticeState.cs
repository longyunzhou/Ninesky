﻿namespace Ninesky.Web.Models
{
    /// <summary>
    /// 提示状态
    /// </summary>
    public class NoticeState
    {
        /// <summary>
        /// 启用
        /// </summary>
        public bool Enable { get; set; }

        /// <summary>
        /// 提示
        /// </summary>
        public NoticeViewModel Notice { get; set; }

        public NoticeState()
        {
            Enable = false;
        }
    }

}
