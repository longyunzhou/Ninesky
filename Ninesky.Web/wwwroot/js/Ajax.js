﻿axios.defaults.baseURL = 'http://localhost:59096/Api';
//axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded';

function GetErrorMessaage(error) {
    var returnData = { status: error.response.status, message: '' };
    //400错误
    if (error.response.status === 400) {
        if (error.response.data instanceof Array) {
            for (var item in error.response.data) {
                returnData.message += item + " " + error.response.data[item];
            }
        } else returnData.message = error.response.data;
    }
    //
    return returnData;
}

var token;


//读取Token
async function ReadToken() {
    if (token != null) return;
    else {
        window.localStorage.removeItem('Token');
        token = window.localStorage.getItem('Token');
        if (token == null) {
            await UserGetToken().then(function (response) {
                token = response.data;
                window.localStorage.setItem('Token', token);
            }).catch(function (error) {
                throw (error);
            });
            
        }
        else {
            var PayloadBase64 = token.split('.')[1];
            var PayloadString = Base64.decode(PayloadBase64);
            var Payload = JSON.parse(PayloadString);
            var expTime = Date(Payload.exp * 1000);
            if ((expTime - new Date()) < 0) {
                await UserGetToken().then(function (response) {
                    token = response.data;
                    window.localStorage.setItem('Token', token);
                }).catch(function (error) {
                    throw (error);
                });
            }
        }
    }
}

//获取Token
function UserGetToken() {
    return axios.get('/User/Token', { withCredentials: true });
}

//添加栏目栏目
function CategoryAdd(data) {
    return axios.post('/Category/Add', data, {
        withCredentials: true,
        headers: {
            'Authorization': 'Bearer ' + token
        }
    });
}

//修改栏目
function CategoryModify(data) {
    return axios.put('/Category/Modify', data, {
        withCredentials: true,
        headers: {
            'Authorization': 'Bearer ' + token
        }
    });
}

//删除栏目
function CategoryDelete(id) {
    return axios.delete('/Category/Delete', {
        withCredentials: true,
        headers: {
            'Authorization': 'Bearer ' + token
        },
        params: {
            id: id
        } 
    });
}

//获取栏目
function CategoryFind(id) {
    return axios.get('/Category/Find', {
        withCredentials: true,
        headers: {
            'Authorization': 'Bearer ' + token
        },
        params: {
            id: id
        } 
    });
}

//获取常规栏目[iviewCascader数据]
function CategoryGetGeneral() {
    return axios.get('/Category/General', {
        withCredentials: true, headers: {
            'Authorization': 'Bearer '+token } });
}

//获取栏目树[iviewTree数据]
function CategoryGetTree() {
    return axios.get('/Category/Tree', {
        withCredentials: true, headers: {
            'Authorization': 'Bearer ' + token
        }
    });
}
