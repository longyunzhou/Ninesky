﻿var CategoryView = {
    template: '#category',
    data: function () {
        return {
            categoryEntity: category,
            parentCategories: [{ label: '无', value: '0' }],
            treedata: [],
            spinData: spinData
        }
    },
    methods:
        {
            typechange: function (model) {
                this.categoryEntity = model;
            },
            renderContent(h, { root, node, data }) {
                let treerenderdata = [];
                switch (data.type) {
                    case 'General':
                        treerenderdata.push(h('Icon', {
                            props: {
                                type: 'folder'
                            },
                            style: {
                                marginRight: '8px',
                                fontSize: '16px'
                            }
                        }));
                        break;
                    case 'Page':
                        treerenderdata.push(h('Icon', {
                            props: {
                                type: 'document'
                            },
                            style: {
                                marginRight: '8px',
                                fontSize: '16px'
                            }
                        }));
                        break;
                    case 'Link':
                        treerenderdata.push(h('Icon', {
                            props: {
                                type: 'link'
                            },
                            style: {
                                marginRight: '8px',
                                fontSize: '16px'
                            }
                        }));
                        break;
                }

                treerenderdata.push(h('span', [
                    h('span', data.title)
                ]));
                //按钮组
                let treerenderbutton = [];
                //添加按钮
                if (data.type === 'General') treerenderbutton.push(h('Button', {
                    props: Object.assign({}, this.buttonProps, {
                        icon: 'android-add'
                    }),
                    on: {
                        click: () => { this.nodeAddClick(data.value) }
                    }
                }, '添加'));
                //修改按钮
                treerenderbutton.push(h('Button', {
                    props: Object.assign({}, this.buttonProps, {
                        icon: 'edit'
                    }),
                    on: {
                        click: () => { this.nodeModifyClick(data.value) }
                    }
                }, '修改'));
                //删除按钮
                if (data.children.length === 0) treerenderbutton.push(h('Button', {
                    props: Object.assign({}, this.buttonProps, {
                        icon: 'android-remove'
                    }),
                    on: {
                        click: () => { this.nodeDeleteClick(data) }
                    }
                }, '删除'));
                let treerenderbuttongroup = h('ButtonGroup', {
                    class: 'ivu-btn-group-small',
                    style: {
                        marginLeft: '16px'
                    }
                }
                    , treerenderbutton);
                treerenderdata.push(treerenderbuttongroup);
                return treerenderdata;
            },
            //添加按钮
            addClick: function () { location.href = '/Admin/Category/Add'; },
            nodeAddClick: function (id) { location.href = '/Admin/Category/Add/' + id; },
            nodeModifyClick: function (id) { location.href = '/Admin/Category/Modify/' + id; },
            nodeDeleteClick: function (nodedata) {
                this.$Modal.confirm({
                    title: '确认',
                    content: '<p>确认要删除栏目【' + nodedata.title + '】吗？</p>',
                    onOk: () => {
                        this.spinData.visible = true;
                        this.spinData.message = '正在删除，请稍等……';
                        _that = this;
                        CategoryDelete(nodedata.value).then(function (response) {
                            CategoryGetTree().then(function (parentCategories) {
                                _that.treedata = parentCategories.data;
                                this.spinData.visible = false;
                            }).catch(function (data) {
                                this.spinData.visible = false;
                                if (data.response.data != undefined) alert(data.response.data);
                            });
                        }).catch(function (data) {
                            this.spinData.visible = false;
                            if (data.response.data != undefined) alert(data.response.data);
                            
                        });
                    }
                });
            }
        },
    updated: function () {
        this.spinData.visible = false;
    },
    created: function () {
        this.spinData.visible = true;
        var _that = this;
        ReadToken().then(function () {
            axios.all([CategoryGetTree()]).then(axios.spread((function (parentCategories) {
                _that.treedata = parentCategories.data;
            })));
        }).catch(function (data) {
            _that.$Modal.error({
                title: '错误',
                content: '连接服务器失败'
            });
        });
    }

}

var vue = new Vue({
    el: '#App',
    data: function () {
        return {
            menuTheme: 'primary'
        };
    },
    components:
        {
            contentView: CategoryView
        }
});