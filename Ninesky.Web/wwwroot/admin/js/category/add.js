﻿//常规栏目
var general = {
    props: ['model', 'parentCategories'],
    template: '#general',
    data: function () {
        var validatorLhp = (rule, value, callback) => {
            if (this.model.type === 'General') {
                let fieldName = '';
                if (rule.field === 'listTemplate') { fieldName = '列表页模板'; }
                else if (rule.field === 'contentTemplate') { fieldName = '内容页模板'; }
                if (value === '') callback(new Error(fieldName + '不能为空'));
                else callback();
            } else {
                callback();
            }
        };
        return {
            type: 'General',
            categoryType: categoryType,
            contentType: contentType,
            spinData: spinData,
            ruleValidate: ruleValidate = {
                parentIds: [{
                    validator: (rule, value, callback) => {
                        if (value === null) callback(new Error('请选择父栏目'));
                        else if (value[value.length - 1] < 0) callback(new Error('父栏目类型错误'));
                        else callback();
                    }, trigger: 'blur'
                }],
                type: [{ required: true, message: '必须选择栏目类型', trigger: 'blur' }, {
                    type: "enum", enum: ['General', 'Page', 'Link'], message: '栏目类型错误', trigger: 'blur'
                }],
                name: [{ required: true, message: '请输入栏目名称', trigger: 'blur' }, {
                    max: 20, message: '最多20个字符', trigger: 'blur'
                }],
                homeTemplate: [{ required: true, message: '请输入首页模板', trigger: 'blur' }],
                listTemplate: [{ validator: validatorLhp, trigger: 'blur' }],
                contentTemplate: [{ validator: validatorLhp, trigger: 'blur' }],
                contentType: [{ type: "enum", enum: ['NotSet', 'Article'], message: '内容类型错误', trigger: 'blur' }],
                meta_Keywords: [{ max: 255, message: '最多255个字符', trigger: 'blur' }],
                meta_Description: [{ max: 255, message: '最多255个字符', trigger: 'blur' }],
                description: [{ max: 500, message: '最多500个字符', trigger: 'blur' }],
                order: [{ required: true, type: 'number', message: '栏目排序必须为数字', trigger: 'blur' }]
            }
        }
    },
    watch: {
        type: function (newQuestion, oldQuestion) {
            this.model.type = newQuestion;
            this.$emit('typechange', this.model);
        },
        'model.parentIds': {
            handler(newQuestion, oldQuestion) {
                this.model.parentId = newQuestion[newQuestion.length - 1];
            },
            deep:true
        }
    },
    methods: {
        handleSubmit(name) {
            var _that = this;
            this.$refs[name].validate((valid) => {
                if (valid) {
                    CategoryAdd(JSON.parse(JSON.stringify(this.model))).then(function (response) {
                        _that.$Modal.success({
                            title: '消息',
                            content: '<p>添加栏目成功</p>',
                            okText: '确定',
                            onOk: () => {
                                window.location.href = "/Admin/Category/Index";
                            }
                        });
                    }).catch(function (error) {
                        let errorData = GetErrorMessaage(error);
                        _that.$Modal.error({
                            title: '错误',
                            content: errorData.message + '(' + errorData.status + ')'
                        });
                    });
                } else {
                    this.$Modal.error({
                        title: '错误',
                        content: '数据验证失败，请检查填写的数据！'
                    });
                }
            })
        }
    }
}
//单页栏目
var page = {
    props: ['model', 'parentCategories'],
    template: '#page',
    data: function () {
        return {
            type: 'Page',
            categoryType: categoryType,
            contentType: contentType,
            spinData: spinData,
            ruleValidate: ruleValidate = {
                parentIds: [{
                    validator: (rule, value, callback) => {
                        if (value == null) callback(new Error('请选择父栏目'));
                        else if (value[value.length - 1] < 0) callback(new Error('父栏目类型错误'));
                        else callback();
                    }, trigger: 'blur'
                }],
                type: [{ required: true, message: '必须选择栏目类型', trigger: 'blur' }, {
                    type: "enum", enum: ['General', 'Page', 'Link'], message: '栏目类型错误', trigger: 'blur'
                }],
                name: [{ required: true, message: '请输入栏目名称', trigger: 'blur' }, {
                    max: 20, message: '最多20个字符', trigger: 'blur'
                }],
                homeTemplate: [{ required: true, message: '请输入首页模板', trigger: 'blur' }],
                meta_Keywords: [{ max: 255, message: '最多255个字符', trigger: 'blur' }],
                meta_Description: [{ max: 255, message: '最多255个字符', trigger: 'blur' }],
                description: [{ max: 500, message: '最多500个字符', trigger: 'blur' }],
                order: [{ required: true, type: 'number', message: '栏目排序必须为数字', trigger: 'blur' }]
            }
        }
    },
    watch: {
        type: function (newQuestion, oldQuestion) {
            this.model.type = newQuestion;
            this.$emit('typechange', this.model);
        }
    },
    methods: {
        handleSubmit(name) {
            var _that = this;
            this.$refs[name].validate((valid) => {
                if (valid) {
                    CategoryAdd(JSON.parse(JSON.stringify(this.model))).then(function (response) {
                        _that.$Modal.success({
                            title: '消息',
                            content: '<p>添加栏目成功</p>',
                            okText: '确定',
                            onOk: () => {
                                window.location.href = "/Admin/Category/Index";
                            }
                        });
                    }).catch(function (error) {
                        let errorData = GetErrorMessaage(error);
                        _that.$Modal.error({
                            title: '错误',
                            content: errorData.message + '(' + errorData.status + ')'
                        });
                    });
                } else {
                    this.$Modal.error({
                        title: '错误',
                        content: '数据验证失败，请检查填写的数据！'
                    });
                }
            })
        }
    }
}
//链接栏目
var link = {
    props: ['model', 'parentCategories'],
    template: '#link',
    data: function () {
        return {
            type: 'Link',
            categoryType: categoryType,
            contentType: contentType,
            spinData: spinData,
            ruleValidate: ruleValidate = {
                parentIds: [{
                    validator: (rule, value, callback) => {
                        if (value === null) callback(new Error('请选择父栏目'));
                        else if (value[value.length - 1] < 0) callback(new Error('父栏目类型错误'));
                        else callback();
                    }, trigger: 'blur'
                }],
                type: [{ required: true, message: '必须选择栏目类型', trigger: 'blur' }, {
                    type: "enum", enum: ['General', 'Page', 'Link'], message: '栏目类型错误', trigger: 'blur'
                }],
                name: [{ required: true, message: '请输入栏目名称', trigger: 'blur' }, {
                    max: 20, message: '最多20个字符', trigger: 'blur'
                }],
                linkUrl: [{ required: true, message: '请输入链接地址', trigger: 'blur' },{
                    max: 255, message: '最多255个字符', trigger: 'blur'
                }],
                description: [{ max: 20, message: '最多500个字符', trigger: 'blur' }],
                order: [{ required: true, type: 'number', message: '栏目排序必须为数字', trigger: 'blur' }]
            }
        }
    },
    watch: {
        type: function (newQuestion, oldQuestion) {
            this.model.type = newQuestion;
            this.$emit('typechange', this.model);
        }
    },
    methods: {
        handleSubmit(name) {
            var _that = this;
            this.$refs[name].validate((valid) => {
                if (valid) {
                    this.spinData.visible = true;
                    this.spinData.message = '正在保存，请稍等……';
                    CategoryAdd(JSON.parse(JSON.stringify(this.model))).then(function (response) {
                        _that.$Modal.success({
                            title: '消息',
                            content: '<p>添加栏目成功</p>',
                            okText: '确定',
                            onOk: () => {
                                window.location.href = "/Admin/Category/Index";
                            }
                        });
                    }).catch(function (error) {
                        _that.spinData.visible = false;
                        let errorData = GetErrorMessaage(error);
                        _that.$Modal.error({
                            title: '错误',
                            content: errorData.message + '(' + errorData.status + ')'
                        });
                    });
                } else {
                    _that.spinData.visible = false;
                    this.$Modal.error({
                        title: '错误',
                        content: '数据验证失败，请检查填写的数据！'
                    });
                }
            })
        }
    }
}

//栏目视图
var CategoryView = {
    template: '#category',
    data: function () {
        return {
            categoryEntity: category,
            parentCategories: [{ label: '无', value: '0' }],
            spinData: spinData
        }
    },
    components: {
        General: general,
        Page: page,
        Link: link
    },
    methods:
        {
            typechange: function (model) {
                this.categoryEntity = model;
            }
        },
    created: function () {
        this.spinData.visible = true;
        var _that = this;
        ReadToken().then(function () {
            axios.all([CategoryGetGeneral()]).then(axios.spread((function (parentCategories) {
                for (let i = 0; i < parentCategories.data.length; i++) {
                    _that.parentCategories.push(parentCategories.data[i]);
                }
            })));
            //获取父栏目
            if (parentId !== '') {
                CategoryFind(parentId).then(function (response) {
                    if (response.status === 200) {
                        let parentStrIds = response.data.parentPath.split(',');
                        _that.categoryEntity.parentIds = [];
                        parentStrIds.forEach(function (item, index, arr) {
                            _that.categoryEntity.parentIds.push(+item);
                        });
                        _that.categoryEntity.parentIds.push(+response.data.categoryId);
                    }
                }).catch(function (error) {
                    this.spinData.visible = true;
                    _that.$Modal.error({
                        title: '错误',
                        content: '查找父栏目时发生错误：' + error.response.data
                    });
                });
            }
        }).catch(function (data) {
            _that.$Modal.error({
                title: '错误',
                content: '连接服务器失败'
            });
        });
    },
    updated: function () {
        this.spinData.visible = false;
    }
}

var vue = new Vue({
    el: '#App',
    data: function () {
        return {
            menuTheme: 'primary'
        };
    },
    components:
        {
            contentView: CategoryView
        }
});