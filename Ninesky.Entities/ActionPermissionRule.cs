﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Ninesky.Entities
{
    /// <summary>
    /// 方法权限
    /// </summary>
    public class ActionPermissionRule
    {
        [Key]
        public int RuleId { get; set; }

        /// <summary>
        /// 控制器
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// 方法
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// 允许匿名
        /// </summary>
        public bool AllowAnonymous { get; set; }
    }
}
