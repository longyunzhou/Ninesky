﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ninesky.Entities
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [Required]
        public UserType Type { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        [StringLength(50)]
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [StringLength(255)]
        public string Password { get; set; }

        /// <summary>
        /// 邮件
        /// </summary>
        [StringLength(255)]
        public string Email { get; set; }

        /// <summary>
        /// 注册时间
        /// </summary>
        [Required]
        public DateTime RegTime { get; set; }

        /// <summary>
        /// 最后登录时间
        /// </summary>
        public DateTime LastLoginTime { get; set; }

        /// <summary>
        /// 最后登录IP
        /// </summary>
        [StringLength(255)]
        public string LastLoginIP { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Required]
        public UserStatus Status { get; set; }
    }


    public enum UserType
    {
        /// <summary>
        /// 匿名
        /// </summary>
        Anonymous,
        /// <summary>
        /// 注册
        /// </summary>
        Registered,
        /// <summary>
        /// 系统
        /// </summary>
        System
    }

    public enum UserStatus
    {
        /// <summary>
        /// 正常
        /// </summary>
        Normal,
        /// <summary>
        /// 锁定
        /// </summary>
        Lock,
        /// <summary>
        /// 未验证
        /// </summary>
        Unverified,
        /// <summary>
        /// 未认证
        /// </summary>
        Unauthorized
    }

}
