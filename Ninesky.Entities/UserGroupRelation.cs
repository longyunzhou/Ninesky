﻿using System.ComponentModel.DataAnnotations;

namespace Ninesky.Entities
{
    /// <summary>
    /// 用户组关系
    /// </summary>
    public class UserGroupRelation
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// 用户I组Id
        /// </summary>
        [Required]
        public int GroupId { get; set; }
    }
}
